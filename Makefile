
progs := lcmfwd

srcs := $(wildcard *.go)
ifdef SNAPSHOT
GR_OPTS := --skip-validate --skip-publish --rm-dist --snapshot
else
GR_OPTS := --skip-validate --skip-publish --rm-dist
endif

BINDIR := $(HOME)/bin
UNITDIR := $(HOME)/.config/systemd/user

all: $(progs)

lcmfwd: .goreleaser.yml $(srcs)
	goreleaser release $(GR_OPTS)

.PHONY: install
install:
	mkdir -p $(BINDIR) $(UNITDIR)
	cp -v $(progs) $(BINDIR)
	cp -v systemd/* $(UNITDIR)
	systemctl --user daemon-reload
