// Lcmfwd forwards LCM messages to a NATS streaming server
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"syscall"

	"bitbucket.org/uwaploe/gss"
	"github.com/lcm-proj/lcm/lcm-go/lcm"
	"github.com/nats-io/stan.go"
)

const Usage = `Usage: lcmfwd [options] lcm_channel [lcm_channel ...]

Forward messages from one or more LCM channels to a NATS stream server. By default, the NATS
subject name is the LCM channel name prepended with "lcm."

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	natsURL   string = "nats://localhost:4222"
	clusterID string = "must-cluster"
	lcmSub    string
)

func parseCommandLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&lcmSub, "subject", lookupEnvOrString("LCM_SUBJECT", lcmSub),
		"NATS subject for LCM messages")

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCommandLine()

	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	status := 0
	defer func() { os.Exit(status) }()

	sc, err := stan.Connect(clusterID, "lcm-forward", stan.NatsURL(natsURL))
	if err != nil {
		log.Fatalf("Cannot connect to NATS: %v", err)
	}
	defer sc.Close()

	lc, err := lcm.New()
	if err != nil {
		log.Printf("Cannot connect to LCM: %v", err)
		status = 1
		return
	}
	defer lc.Destroy()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			lc.UnsubscribeAll()
		}
	}()

	ch := make(chan gss.Event, len(args))
	defer close(ch)

	var wg sync.WaitGroup

	for _, arg := range args {
		sub, err := lc.Subscribe(arg, 5)
		if err != nil {
			log.Printf("Cannot subscribe to %q: %v", args[0], err)
			status = 1
			return
		}
		wg.Add(1)
		go func(sub lcm.Subscription) {
			defer wg.Done()
			var i uint64
			for data := range sub.ReceiveChan {
				ev := gss.NewEvent(sub.Channel(), i, data)
				ch <- ev
				i++
			}
		}(sub)
	}

	if lcmSub == "" {
		go func() {
			for ev := range ch {
				b, _ := ev.MarshalBinary()
				sc.Publish("lcm."+ev.Channel(), b)
			}
		}()
	} else {
		ev_index := uint64(0)
		go func() {
			for ev := range ch {
				ev.Event = ev_index
				b, _ := ev.MarshalBinary()
				sc.Publish(lcmSub, b)
				ev_index++
			}
		}()
	}

	wg.Wait()
}
