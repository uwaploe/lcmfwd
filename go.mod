module bitbucket.org/uwaploe/lcmfwd

go 1.13

require (
	bitbucket.org/uwaploe/gss v1.3.0
	github.com/lcm-proj/lcm v1.4.0
	github.com/nats-io/stan.go v0.6.0
)
